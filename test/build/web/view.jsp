<%-- 
    Document   : result
    Created on : Feb 27, 2018, 3:01:32 PM
    Author     : Puarinnnn
--%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>List Product!</title>
        <link href="css/newcss.css" rel="stylesheet" type="text/css"/>
        <script src="js/newjavascript.js" type="text/javascript"></script>
        <link href="css/cssButton.css" rel="stylesheet" type="text/css"/>
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    </head>
    <h1>List Product!</h1>
    <a href="search.jsp">Search</a>
    <a href="add.jsp">add</a>
    <a href="view.jsp">view</a>
    <jsp:useBean class="com.wpsj.model.getAll" id="finder1" scope="request"/>
    <div class="tbl-header">
        <table cellpadding="0" cellspacing="0" border="0">
            <thead>
                <tr>
                    <th>Id</th>
                    <th>Name</th>
                    <th>Desc</th>
                    <th>Edit</th>
                    <th>Delete</th>
                </tr>
            </thead>
        </table>
    </div>
    <div class="tbl-content">
        <table cellpadding="0" cellspacing="0" border="0">
            <c:forEach items="${finder1.products}" var="product">
                <tr>
                    <td><c:out value="${product.id}"/></td>
                    <td><c:out value="${product.name}"/></td>
                    <td><c:out value="${product.desc}"/></td>
                    <td> 
                        <div class="buttons">
                            <a href="edit?code=${product.id}" class="button edit"><span>Edit</span></a>
                        </div>
                    </td>
                    <td>

                        <div class="buttons">
                            <a href="delete?code=${product.id}" class="button delete"><span>Delete</span></a>
                        </div>

                    </td>


                </tr>
            </c:forEach>
        </table></div>

</html>
