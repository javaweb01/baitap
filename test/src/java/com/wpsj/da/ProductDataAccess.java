/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.wpsj.da;

import com.wspj.entity.Product;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.LinkedList;
import java.util.List;

/**
 *
 * @author Puarinnnn
 */
public class ProductDataAccess {

    private PreparedStatement searchStatement;

    private PreparedStatement getSearchStatement() throws ClassNotFoundException, SQLException {
        if (searchStatement == null) {
            Connection connection = new DBConnection().getConnection();
            searchStatement = connection.prepareStatement("SELECT pro_id, pro_name, pro_desc FROM ProductStore WHERE pro_name like ?");
        }
        return searchStatement;
    }

    public List<Product> getProductsByName(String name) {
        try {
            PreparedStatement statement = getSearchStatement();
            statement.setString(1, "%" + name + "%");
            ResultSet rs = statement.executeQuery();
            List<Product> products = new LinkedList<Product>();
            while (rs.next()) {
                products.add(new Product(rs.getInt("pro_id"),
                        rs.getString("pro_name"), rs.getString("pro_desc")));
            }
            return products;

        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public boolean add(int id, String name, String desc) {
        try {
            Connection connection = new DBConnection().getConnection();
            PreparedStatement psInsert = connection
                    .prepareStatement("insert into ProductStore values (?,?,?)");

            psInsert.setInt(1, id);
            psInsert.setString(2, name);
            psInsert.setString(3, desc);

            psInsert.executeUpdate();
            return true;
        } catch (Exception e) {
            e.printStackTrace();
      return false;
        }
    }

    public boolean delete(int id) {
        try {
            Connection connection = new DBConnection().getConnection();
            PreparedStatement psInsert = connection
                    .prepareStatement("DELETE FROM PRODUCTSTORE WHERE PRO_ID=?");

            psInsert.setInt(1, id);

            psInsert.executeUpdate();
            return true;
        } catch (Exception e) {
            e.printStackTrace();
return false;
        }
    }

    public boolean UPDATE(int id, String name, String desc) {
        try {
            Connection connection = new DBConnection().getConnection();
            PreparedStatement psInsert = connection
                    .prepareStatement("UPDATE PRODUCTSTORE SET PRO_NAME = ?, PRO_DESC = ? WHERE PRO_ID = ?");

            psInsert.setInt(3, id);
            psInsert.setString(1, name);
            psInsert.setString(2, desc);
            psInsert.executeUpdate();
             return true;
        } catch (Exception e) {
            e.printStackTrace();

        }
        return false;
    }

    public List<Product> getall() {

        try {
            Connection connection = new DBConnection().getConnection();
            Statement stmt2 = connection.createStatement();
            ResultSet rs = stmt2.executeQuery("select * from ProductStore");
            List<Product> products = new LinkedList<Product>();
            while (rs.next()) {
                products.add(new Product(rs.getInt(1),
                        rs.getString(2), rs.getString(3)));
            }
            return products;

        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public Product getProductId(int id) {

        try {
            Connection connection = new DBConnection().getConnection();
            Statement stmt2 = connection.createStatement();
            ResultSet rs = stmt2.executeQuery("select * from ProductStore WHERE PRO_ID=" + id);
            Product product = new Product();
            while (rs.next()) {

                product.setId(rs.getInt(1));
                product.setName(rs.getString(2));
                product.setDesc(rs.getString(3));
            }
            return product;

        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

}
